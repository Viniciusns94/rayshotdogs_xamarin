﻿using RaysHotDogs.Core.model;
using RaysHotDogs.Core.Model;
using RaysHotDogs.Core.repository;
using System.Collections.Generic;

namespace RaysHotDogs.Core.Service
{
    public class HotDogDataService
    {
        private static HotDogRepository hotDogRepository = new HotDogRepository();

        public List<HotDog> GetAllHotDogs()
        {
            return hotDogRepository.GetAllHotDogs();
        }

        public List<HotDogGroup> GetGroupedHotDogs()
        {
            return hotDogRepository.GetGroupedHotDogs();
        } 

        public List<HotDog> GetHotDogsForGroup(int hotDogGroupId)
        {
            return hotDogRepository.GetHotDogsForGroup(hotDogGroupId);
        }

        public List<HotDog> GetFavoriteHotDogs()
        {
            return hotDogRepository.GetFavoriteHotDogsForGroup();
        }

        public HotDog GetHotDogById(int idHotDogId)
        {
            return hotDogRepository.GetHotById(idHotDogId);
        }
    }
}

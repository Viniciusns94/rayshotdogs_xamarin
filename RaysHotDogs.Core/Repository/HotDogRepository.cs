﻿using Newtonsoft.Json;
using RaysHotDogs.Core.model;
using RaysHotDogs.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RaysHotDogs.Core.repository
{
    public class HotDogRepository
    {
        private static readonly List<HotDogGroup> hotDogGroups = new List<HotDogGroup>()
        {
            new HotDogGroup()
            {
                HotDogGropId = 1, Title = "Hot Dogs Carne", ImagePath ="", HotDogs = new List<HotDog>()
                {
                    new HotDog(){
                        HotDogId = 1,
                        Name = "Simples",
                        ShortDescription = "Barato e rápido",
                        Description = "Custo baixo e rápido de se fazer",
                        ImagePath = "",
                        Avaliable = true,
                        PrepTime = 10,
                        Ingredients = new List<string>(){
                            "Pão","Salcicha","Batata Palha","Purê","Presada"
                        },
                        Price = 5.00,
                        IsFavorite = true
                    },
                    new HotDog(){
                        HotDogId = 2,
                        Name = "Simples turbo",
                        ShortDescription = "Custo benefício",
                        Description = "Custo baixo e razuavelmente rápido",
                        ImagePath = "",
                        Avaliable = true,
                        PrepTime = 15,
                        Ingredients = new List<string>(){
                            "Pão","2 Salcichas","Batata Palha","Purê","Presada","Alface","Tomate"
                        },
                        Price = 8.00,
                        IsFavorite = false
                    },
                    new HotDog(){
                        HotDogId = 3,
                        Name = "TOP",
                        ShortDescription = "Satisfação garantida",
                        Description = "Feito para quem esta com buraco no estomago",
                        ImagePath = "",
                        Avaliable = true,
                        PrepTime = 20,
                        Ingredients = new List<string>(){
                            "Pão especial","2 Salcichas","Batata Palha","Purê","Presada","Alface","Tomate","Ervilha","Milho"
                        },
                        Price = 10.00,
                        IsFavorite = false
                    }
                }
            },

            new HotDogGroup()
            {
                HotDogGropId = 2, Title = "Hot Dogs Veganos", ImagePath = "", HotDogs = new List<Model.HotDog>()
                {
                    new HotDog(){
                        HotDogId = 4,
                        Name = "Simples salcicha de batata",
                        ShortDescription = "Barato e rápido",
                        Description = "Custo baixo e rápido de se fazer",
                        ImagePath = "",
                        Avaliable = true,
                        PrepTime = 8,
                        Ingredients = new List<string>(){
                            "Pão","Salcicha","Batata Palha","Purê"
                        },
                        Price = 3.00,
                        IsFavorite = false
                    },
                    new HotDog(){
                        HotDogId = 5,
                        Name = "Simples turbo salcicha de melancia",
                        ShortDescription = "Custo benefício",
                        Description = "Custo baixo e razuavelmente rápido",
                        ImagePath = "",
                        Avaliable = true,
                        PrepTime = 13,
                        Ingredients = new List<string>(){
                            "Pão","2 Salcichas","Batata Palha","Purê","Alface","Tomate"
                        },
                        Price = 5.00,
                        IsFavorite = true
                    },
                    new HotDog(){
                        HotDogId = 6,
                        Name = "TOP salcicha de abaxi",
                        ShortDescription = "Satisfação garantida",
                        Description = "Feito para quem esta com buraco no estomago",
                        ImagePath = "",
                        Avaliable = true,
                        PrepTime = 15,
                        Ingredients = new List<string>(){
                            "Pão especial","2 Salcichas","Batata Palha","Purê","Alface","Tomate","Ervilha","Milho"
                        },
                        Price = 8.00,
                        IsFavorite = false
                    }
                }
            }
        };

        //retorna todos os hotDogs

        //private static List<HotDogGroup> hotDogGroups = new List<HotDogGroup>();

        //string url =
        //    "http://gillcleerenpluralsight.blob.core.windows.net/files/hotdogs.jason";

        //public HotDogRepository()
        //{
        //    Task.Run(() => this.LoadDataAsync(url)).Wait();
        //}

        //private async Task LoadDataAsync(string uri)
        //{
        //    if(hotDogGroups != null)
        //    {
        //        string responseJsonString = null;

        //        using (var httpClient = new HttpClient())
        //        {
        //            try
        //            {
        //                Task<HttpResponseMessage> getResponse = httpClient.GetAsync(uri);

        //                HttpResponseMessage response = await getResponse;

        //                responseJsonString = await response.Content.ReadAsStringAsync();

        //                hotDogGroups = JsonConvert.DeserializeObject<List<HotDogGroup>>(responseJsonString);
        //            }
        //            catch (Exception ex)
        //            {
        //                throw;
        //            }
        //        }
        //    }
        //}

        public List<HotDog> GetAllHotDogs()
        {
            IEnumerable<HotDog> hotDogs = from hotDogGroup in hotDogGroups
                                          from hotDog in hotDogGroup.HotDogs
                                          select hotDog;
            return hotDogs.ToList<HotDog>();
        }

        //retorna um hotDog instanciado pelo id
        public HotDog GetHotById(int hotDogId)
        {
            IEnumerable<HotDog> hotDogs = from hotDogGroup in hotDogGroups
                                          from hotDog in hotDogGroup.HotDogs
                                          where hotDog.HotDogId == hotDogId
                                          select hotDog;
            return hotDogs.FirstOrDefault();
        }

        //retorna todos os grupos de hotDogs
        public List<HotDogGroup> GetGroupedHotDogs()
        {
            return hotDogGroups;
        }

        //retorna um grupo de hotDogs instanciado pelo id
        public List<HotDog> GetHotDogsForGroup(int hotDogGroupId)
        {
            var group = hotDogGroups.Where(h => h.HotDogGropId == hotDogGroupId).FirstOrDefault();

            if(group != null)
            {
                return group.HotDogs;
            }
            return null;
        }

        //retorna os hotDogs favoritos
        public List<HotDog> GetFavoriteHotDogsForGroup()
        {
            IEnumerable<HotDog> hotDogs = from hotDogGroup in hotDogGroups
                                          from hotDog in hotDogGroup.HotDogs
                                          where hotDog.IsFavorite
                                          select hotDog;

            return hotDogs.ToList<HotDog>();
        }
    };
}

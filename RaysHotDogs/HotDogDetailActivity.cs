﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using RaysHotDogs.Core.Model;
using RaysHotDogs.Core.Service;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace RaysHotDogs
{
    [Activity(Label = "Detalhes dos Hot Dogs", Theme= "@style/AppTheme.NoActionBar")]
    public class HotDogDetailActivity : AppCompatActivity
    {
        private ImageView hotDogImageView;
        private TextView hotDogNameTextView;
        private TextView shortDescriptionTextView;
        private TextView descriptionTextView;
        private TextView priceTextView;
        private EditText qtdAmountEditText;
        private Button cancelButton;
        private Button orderButton;

        private HotDog selectedHotDog;
        //private HotDogDataService dataService;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_hot_dog_detail);

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar_detail);
            toolbar.SetTitleTextColor(Android.Graphics.Color.White);
            SetSupportActionBar(toolbar);

            HotDogDataService dataService = new HotDogDataService();           
            var selectedHotDogId = Intent.Extras.GetInt("HOT_DOG_SELETED_ID");
            selectedHotDog = dataService.GetHotDogById(selectedHotDogId);

            FindViews();
            BindData();
            HandleEvents();
        }

        private void FindViews()
        {
            hotDogImageView = FindViewById<ImageView>(Resource.Id.hotDogImageView);
            hotDogNameTextView = FindViewById<TextView>(Resource.Id.hotDogNameTextView);
            shortDescriptionTextView = FindViewById<TextView>(Resource.Id.shortDescriptionTextView);
            descriptionTextView = FindViewById<TextView>(Resource.Id.descriptionTextView);
            priceTextView = FindViewById<TextView>(Resource.Id.priceTextView);
            qtdAmountEditText = FindViewById<EditText>(Resource.Id.amountEditText);
            cancelButton = FindViewById<Button>(Resource.Id.cancelButton);
            orderButton = FindViewById<Button>(Resource.Id.orderButton);
        }

        private void BindData()
        { 
            hotDogNameTextView.Text = selectedHotDog.Name;
            shortDescriptionTextView.Text = selectedHotDog.ShortDescription;
            descriptionTextView.Text = selectedHotDog.Description;
            priceTextView.Text = "Price: "+selectedHotDog.Price;

            //var imageBitmap = ImageHelper.GetImageBitmapFromUrl("http://gillcleerenpluralsinght.blob.core.windows.net/files/" + selectedHotDog.ImagePath + ".jpg");
            //hotDogImageView.SetImageBitmap(imageBitmap);    
        }
            
        private void HandleEvents()
        {
            orderButton.Click += OrdemButtonClick;
            cancelButton.Click += CancelButtonClick;
        }

        private void CancelButtonClick(object sender, EventArgs e)
        {
            //TODO
        }

        private void OrdemButtonClick(object sender, EventArgs e)
        {
            var amount = Int32.Parse(qtdAmountEditText.Text);

            var intent = new Intent();
            intent.PutExtra("HOT_DOG_SELETED_ID", selectedHotDog.HotDogId);
            intent.PutExtra("QTD", amount);

            SetResult(Result.Ok, intent);//chama a activity pai independente qual, passando o código de resultado Result.OK e o conteúdo da intent

            this.Finish();
        }
    }
}
﻿using Android.OS;
using Android.Views;
using RaysHotDogs.Adapters;

namespace RaysHotDogs.Fragments
{
    public class FavoriteHotDogFragment : BaseFragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            FinViews();

            HandleEvents();

            hotDogs = hotDogDataService.GetFavoriteHotDogs();
            listView.Adapter = new HotDogListAdapter(this.Activity, hotDogs);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.fragment_favorite_hot_dog, container, false);
        }
    }
}
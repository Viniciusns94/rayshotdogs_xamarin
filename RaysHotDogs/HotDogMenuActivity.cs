﻿
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using RaysHotDogs.Core.Model;
using RaysHotDogs.Core.Service;
using RaysHotDogs.Fragments;
using System.Collections.Generic;

namespace RaysHotDogs
{
    [Activity(Label = "HotDogMenuActivity", Theme = "@style/MyThemeActionBar")]
    public class HotDogMenuActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);  

            SetContentView(Resource.Layout.activity_hot_dog_menu_view);

            ActionBar.SetIcon(Android.Resource.Color.Transparent);

            ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;

            AddTab("Favoritos", Resource.Drawable.FavoritesIcon, new FavoriteHotDogFragment());
            AddTab("Amantes de carne", Resource.Drawable.MeatLoversIcon, new MeatLoversFragment());
            AddTab("Veganos", Resource.Drawable.VeggieLoversIcon, new VeggieLoversFragment());
        }

        private void AddTab(string tabText, int iconResourceId, Fragment view)
        {
            var tab = this.ActionBar.NewTab();
            tab.SetText(tabText);
            tab.SetIcon(iconResourceId);

            tab.TabSelected += delegate (object sender, ActionBar.TabEventArgs e)
            {
                var fragment = this.FragmentManager.FindFragmentById(Resource.Id.fragmentContainer);
                if (fragment != null)
                    e.FragmentTransaction.Remove(fragment);
                e.FragmentTransaction.Add(Resource.Id.fragmentContainer, view);
            };

            tab.TabUnselected += delegate (object sender, ActionBar.TabEventArgs e)
            {
                e.FragmentTransaction.Remove(view);
            };

            this.ActionBar.AddTab(tab);
        }
    }
}
﻿using System.Collections.Generic;

using Android.App;
using Android.Views;
using Android.Widget;
using RaysHotDogs.Core.Model;
using RaysHotDogs.Utility;

namespace RaysHotDogs.Adapters
{
    public class HotDogListAdapter : BaseAdapter<HotDog>
    {
        List<HotDog> items;
        Activity context;

        public HotDogListAdapter(Activity context, List<HotDog> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position; 
        }

        public override HotDog this[int position]
        {
            get
            {
                return items[position];
            }
        }

        public override int Count
        {
            get
            {
                return items.Count;
            }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            //var imageBitmap = ImageHelper.GetImageBitmapFromUrl("http://gillcleerenpluralsinght.blob.core.windows.net/files/" + ".jpg");

            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.hot_dog_linha_view, null);
            }
            convertView.FindViewById<TextView>(Resource.Id.hotDogNameTextView_list).Text = item.Name;
            convertView.FindViewById<TextView>(Resource.Id.shortDescriptionTextView_list).Text = item.ShortDescription;
            convertView.FindViewById<TextView>(Resource.Id.priceTextView_list).Text = "R$: " + item.Price;
            //convertView.FindViewById<ImageView>(Resource.Id.hotDogImageView_list).SetImageBitmap(imageBitmap); 

            return convertView;
        }
    }
}
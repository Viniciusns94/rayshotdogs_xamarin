﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace RaysHotDogs
{
    [Activity(Label = "RayMapActivity", Theme = "@style/AppTheme.NoActionBar")]
    public class RayMapActivity : AppCompatActivity
    {
        private Button externalMapButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_ray_map_view);

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar_menu);
            toolbar.SetTitleTextColor(Android.Graphics.Color.White);
            SetSupportActionBar(toolbar);

            FindViews();

            HandleEvents();
        }

        private void FindViews()
        {
            externalMapButton = FindViewById<Button>(Resource.Id.externalMapButton);
        }

        private void HandleEvents()
        {
            externalMapButton.Click += ExternarMapButton_Click;
        }

        private void ExternarMapButton_Click(object sender, EventArgs e)
        {
            Android.Net.Uri rayLoctionUri =
                Android.Net.Uri.Parse("geo:50.846704,4.352446");
            var mapIntent = new Intent(Intent.ActionView, rayLoctionUri);
            StartActivity(mapIntent);
        }
    }
}
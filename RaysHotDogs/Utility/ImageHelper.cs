﻿using System.Net;
using Android.Graphics;

namespace RaysHotDogs.Utility
{
    public class ImageHelper
    {
        public static Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }
            return imageBitmap;
        }

        public static Bitmap GetImageBitmapFromFilePath(string fileName, int width, int heigth)
        {
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true};
            BitmapFactory.DecodeFile(fileName, options);

            int outHeigth = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if(outHeigth > heigth || outWidth > width)
            {
                inSampleSize = outWidth > outHeigth
                    ? outHeigth / heigth
                    : outWidth / width;
            }

            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);
            
            return resizedBitmap;
        }
    }   
}